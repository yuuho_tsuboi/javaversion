# NAME

JavaVersion - It's new $module

# SYNOPSIS

    use JavaVersion;

# DESCRIPTION

JavaVersion is ...

# LICENSE

Copyright (C) yuuho.tsuboi.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

# AUTHOR

yuuho.tsuboi <yuuho.tsuboi@adish.co.jp>
