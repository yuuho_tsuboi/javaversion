package JavaVersion;
use 5.008001;
use strict;
use warnings;

our $VERSION = "0.01";



1;
__END__

=encoding utf-8

=head1 NAME

JavaVersion - It's new $module

=head1 SYNOPSIS

    use JavaVersion;

=head1 DESCRIPTION

JavaVersion is ...

=head1 LICENSE

Copyright (C) yuuho.tsuboi.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=head1 AUTHOR

yuuho.tsuboi E<lt>yuuho.tsuboi@adish.co.jpE<gt>

=cut

